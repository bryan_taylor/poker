## Preflop Raise First In strategy

**1. When it is unopened to you, preflop, what factors influence if you will raise, call, or fold?**

 - Hand Strength
   - We generally open raise with strong hands that are ahead a lot
   - We may open raise with some hands whose equity comes from drawing to big hands
   - We think of hand strength in terms of "equity when called"
 - Position
   - I generally only open limp in the SB
   - The BB can consider checking or raising if limped to
   - All other positions will open raise or fold
 - Stack-to-Pot ratio (SPR)
   With Deeper stacks, we our implied odds for drawing hands that hit big, but rarely work better. We tend to play slighly more hands as we can set mine and play suited connectors and aces slightly more boldly.
 - Tendencies of Players still to act
   - In the cutoff, button, or SB positions we may raise wider if our steal is more likely to succeed against players to follow, especially the BB.
   - If players behind us don't 3bet much, we may play a few more weaker pairs and drawing hands (connectors and suited aces) that we avoid because we'd fold them to a 3bet
   - Against overly strong preflop aggression, we may tighten our RFI ranges, but widen our 4bet ranges.


**2. What are your opening ranges: Raise First In and limp ranges by seat:**
- UTG
- HJ
- CO
- BTN


I mostly follow the [implementable GTO](https://poker-coaching.s3.amazonaws.com/tools/preflop-charts/online-6max-gto-charts.pdf) ranges. These are an approximation to GTO, taken to simplify play so there are no hands with mixed strategies while remaining balanced.

I play the positions mentioned using this simplified chart:
<br/><img src="images/rfi-all.png" alt="rfi-all" width="400" height="600"/>

**3. Exercise: Go to the [Range Analyzer](https://pokercoaching.com/range-analyzer/) and draw your UTG/HJ/CO/BTN ranges in red/blue/green/black. Score yourself 100 minus 1 for each hand/street combo you would play wrong and give yourself a letter grade.**

10/26/2021 - Missed 98s, 76s, 74s, each off 1 street. Score == 97. Grade = A


**4. What stats does a HUD gather that characterize a player's preflop play?**

I like the following stats:
 - VPIP (voluntarily put in put) - what percent of hands did a player add money to the pot volutarily by raising or calling (not including checking as BB)
 - PFR (preflop raise) - percent of hands where hero raised preflop
 - RFI% - percent of hands where hero raised first in when they could
 - 3bet
 - fold-to-3bet
 - 2bet PF & Fold
 - 4bet
 - fold-to-4bet
 - 3bet PF & Fold
 - Attempt-steal
 - Fold-to-steal

**5. Exercise: user your HUD to find your values for these stats**

  As of 10/26 for 3294 Hands played this month:
  - VPIP/PRF: 26.7 / 21.5
  - RFI%: 28.0%
  - 3bet / Fold to 3bet / 2bet PF & Fold: 10.5 / 57.7 / 23.8
  - 4bet / Fold to 4bet / 3bet PF & Fold: 8.78 / 35.9 / 25.9
  - Attempt Steal / Fold to Steal: 39.79 / 64.6

**6. What betsize do you use for your raises?**
  - RFI UTG,HJ,CO,BTN: 2.5 BB
  - RFI SB: 3.5 BB
  - 3bet In position (IP): 3X raise
  - 3bet Out of position (OOP): 4X raise

**7. Define your raise/call/fold strategy facing an RFI for each of the following situations:**
  - Early/Middle:
    - HJ facing UTG RFI
    - CO facing UTG RFI
    - CO facing HJ RFI
<br/><img src="images/facing_rfi_hj-co.png" alt="rfi-all" width="400" height="600"/>

 - Button:
    - BTN facing UTG RFI
    - BTN facing HJ RFI
    - BTN facing CO RFI
<br/><img src="images/facing_rfi_btn.png" alt="rfi-all" width="400" height="700"/>
 - Small Blind:
    - SB facing UTG RFI
    - SB facing HJ RFI
    - SB facing CO RFI
    - SB facing BTN RFI
<br/><img src="images/facing_rfi_sb.png" alt="rfi-all" width="400" height="600"/>


**8. Exercise: Quiz yourself over early/middle, BTN and SB play facing an RFI with the Range Analyzer scoring as per #3**

**9. If you are the SB and it's folded to you, how do you open? What are your RFI and limp ranges?**

**10. How do you respond to limpers in front of you? As HJ, CO, BTN, SB?**

I will raise with my RFI ranges as above, but add 1BB per limper to the bet sizing.

**11. Define your Big Blind play call/raise ranges for:**
   - BB facing UTG RFI
   - BB facing HJ RFI
   - BB facing CO RFI
   - BB facing BTN RFI
   - BB facing SB RFI
   - BB facing SB open limp
   - BB facing other, possibly multiple limps

Here is combined range for BB facing UTG/HJ/CO/BTN RFI:
<br/><img src="images/facing_rfi_bb.png" alt="rfi-all" width="400" height="600"/>

This isn't fully specified because the green color is "varies". Work TBD.

TBD for blind vs blind and BB vs multiple limpers.

**12. Exercise: Quiz yourself over BB ranges with the Range Analyzer, scoring as per #3**

TBD

**13. Exercise: Using your poker tracking software identify all the hands from a session where:**
  - You were RFI when you shouldn't have been
  - You folded or limped when you should've been RFI
  - You called an RFI when you should've folded

I did this in PokerTracker 4 by going to View Stats -> Statistics and selecting "Holdem Hand Range Visualizer". I filtered for the current month and wQent through the positions in question, comparing to my desired ranges. It's helpful to put the heat map on RFI for the first two and "Value". For bad calls, you can put the heat map on "Cold Call 2bet".

Here's what I found, when I did this for Oct-2021:
  - RFI when fold was correct:
    - UTG: KTo, 55-22, A2s,  98s, 87s, Q7s, Q6s, J6s, QTo, A9
    - HJ: 44-22, K3s, Q7s, T8s, T6s, 75s, 65s, 54s
    - CO: 22, A7o, K9o, A6o, A2o, K2s, Q3s, J2s, Q9o, J9o, T9o, 98o, 87o, 86o
    - BTN: A2o, K6o, K4o, K3o, K2o, Q8o, Q7o, J8o, J7o, J6o, T3o, 75o, J3s, T5s, 94s, 74s, 52s, 43s
  - Didn't RFI when RFI was correct:
    - UTG: KK, QTs
    - HJ: K6, 55
    - CO: A2s, K3s, A9o, A8o, J8s, T8s, T7s
    - BTN: K9o, 98o, T8o, Q5s
  - Called when fold was correct:
    - HJ: KJo, QJo, 44-33
    - CO: 77-55, 75s, K9s
    - BTN: A2s, 33-22, A9o-A7o, A5o, JTo, Q9o, K8o

**14. Define these preflop HUD statistics:**
  - VPIP (voluntarily put in pot)
  - PFR (preflop raise)
  - RFI% (raise first in percentage)
  - Attempt to Steal %
  - Fold to Steal %

**15. Exercise: For your HUD, find out how to see the above stats during play (or review), including by position.**

I'm using PT4. VPIP and PRF are on the main HUD for each player. You can see RFI total and by position on the tools tab of the popup. I customized my HUD and added Fold to Steal % to the main HUD for each player.

You can get a position report by going to View Stats->Statistics, Report->Summary, Group->Position.

Here's my values for Oct-2021:
  - UTG: 23/23   rfi=22.8%
  - HJ:  22/21.5 rfi=21.5%
  - CO:  27/26   rfi=30.9%   AS=30.9%
  - BTN: 37/31   rfi=45.44%  AS=45.4%
  - SB:  36/29   rfi=51.72%  AS=43.8% FS=51.7%
  - BB:  35/14   FS=79.8
