# Equity Setups
## Study Template

This is a markdown template adapted from Sky Matsuhashi's [7-Step Process](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/) for getting the most from poker training content. Source code is [here](https://gitlab.com/bryan_taylor/poker).


## 1. Skim
  Skim the content, identify major concepts

**Book**: Poker Workbook Math & Preflop, by James "SplitSuit" Sweeney

**Chapter**: Equity Setups

**Concepts**:
 - using tools to calculate equity
 - hand-vs-hand equity
 - hand-vs-range equity
 - range-vs-range equity


## 2. Goals
  Set chapter goals.

Questions:
 - What skills can I learn from this chapter?
   - How to use tools to calculate equity
 - Why are they important or relevant to my game?
   - Equity is an important part of EV evaluation, which decides what actions are best.
 - How can I implement them in my game?
   - We can imagine hands our opponent can have and estimate the equity based on expereience and common scenarios
   - We can model opponent ranges and apply equity of hand vs range situations

## 3. Read and Watch
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic from books, videos, blogs or other sources.

**Date finished:** 4/9/2023
**Output list:**
 - Hand vs Hand equities: calculations using [openpokertools.com](https://openpokertools.com/range-equity.html)
   - AhAs vs KdKh guess 81% actual 82%
   - AdAc vs AhKs guess 91% actual 93%
   - AdAs vs Td9d guess 72% actual 78%
   - AsAh vs Ac5c guess 75% actual 87%
   - AsAd vs Th4c guess 80% actual 86%
   - AcAs vs QhJh guess 75% actual 80%
   - KhKc vs AcKd guess 70% actual 70%
   - KdKc vs AsKs guess 74% actual 66%
   - KdKs vs Ac4c guess 67% actual 67%
   - KhKs vs Td9d guess 75% actual 78%
   - KsKd vs 8d5h guess 85% actual 84%
   - AsKs vs AdAh guess 10% actual 12%
   - AhKs vs QdQh guess 48% actual 46%
   - AdKh vs AcQs guess 72% actual 76%
   - AsKc vs Th9h guess 65% actual 59%
   - AhKd vs AsKs guess 48% actual 48% (2.2% win 90.7% chop)
   - AsKc vs KdQd guess 66% actual 70%
   - AhKs vs 8d8c guess 48% actual 44%
   - QsTs vs 7s7d guess 48% actual 50%
   - Tc9c vs 9d8d guess 66% actual 69%
   - AcTh vs KsQd guess 56% actual 60%
   - AhQs vs KdJd guess 55% actual 59%
   - Ac5c vs 7h7c guess 30% actual 34%
   - Ad3d vs As6s guess 40% actual 43%
 - Hand vs Range
   - KdKc vs AA-JJ,AKs,AKo guess 68% actual 63%
   - AcKh vs AA-JJ,AKs,AKo guess 40% actual 40%
   - QhQd vs AA-JJ,AKs,AKo guess 45% actual 47%
   - TdTs vs AA-JJ,AKs,AKo guess 33% actual 34%
   - AcKh vs AA-88,AKs-ATs,AKo-AQo guess 57% actual 51%
   - ThTd vs AA-88,AKs-ATs,AKo-AQo guess 47% actual 48%
   - AcKh vs any two cards guess 67% actual 65%
   - 7c7d vs any two cards guess 64% actual 66%
 - Range vs Range
   - 22+,A2s+,K2s+,Q2s+,J5s+,T6s+,96s+,85s+,74s+,64s+,53s+,43s,A2o+,K7o+,Q9o+,J9o+,T8o+,98o,87o vs 99-22,A9s-A6s,K9s-K6s,QTs-Q9s,JTs-J9s,T9s-T8s,98s-97s,87s-86s,76s-75s,65s,54s,ATo-A8o,KJo-KTo,QJo-QTo,JTo guess 56% actual 49.6%
   - 22+,A2s+,K2s+,Q2s+,J5s+,T6s+,96s+,85s+,74s+,64s+,53s+,43s,A2o+,K7o+,Q9o+,J9o+,T8o+,98o,87o vs 88-22,K5s-K2s,Q8s-Q2s,J9s-J5s,T5s+,95s+,85s+,74s+,63s+,52s+,43s,A9o-A2o,KTo-K2o,Q3o+,J7o+,T7o+,97o+,87o,76o,65o guess 58% actual 56%
   - 55+,ATs+,A5s-A2s,KTs+,QTs+,J9s+,T9s,98s,87s,76s,AJo+,KQo vs QQ-22,AJs-A2s,KTs+,QTs+,JTs,T9s,98s,87s,76s,65s,54s,AQo-AJo,KQo guess 60% actual 54%
   - 55+,ATs+,A5s-A2s,KTs+,QTs+,J9s+,T9s,98s,87s,76s,AJo+,KQo vs TT-22,A9s-A6s,KJs-K2s,Q8s+,J8s+,T8s+,97s+,86s+,75s+,64s+,53s+,43s,AJo-ATo,KJo+ guess 56% actual 59%
   - TT-22,AQs-A6s,K8s+,Q9s+,J9s+,T8s+,97s+,86s+,76s,65s,54s vs JJ+,AJs+,AQo+, A4s-A2s,K8s-K6s,Q8s-Q7s,J8s-J7s,T8s-T7s,97s,86s,75s+,65s,54s,43s guess 55% actual 47%
   - TT-22,AQs-A6s,K8s+,Q9s+,J9s+,T8s+,97s+,86s+,76s,65s,54s vs TT+,ATs+,KQs,AJo+, K4s-K2s,Q7s-Q2s,J7s-J6s,T7s,97s-96s,86s-85s,75s+,65s,54s,43s guess 46% actual 48%
   - TT+,ATs+,A5s-A2s,KQs,AJo+,KQo,KJs-KTs,QJs vs TT-22,AQs-A6s,K8s+,Q9s+,J9s+,T8s+,97s+,86s+,76s,65s,54s guess 56% actual 60%
   - TT+,ATs+,A5s-A2s,KQs,AJo+,KQo,KJs-KTs,QJs vs AA,JJ-55,AQs-A9s,KQs-KTs,QJs-QTs,JTs,T9s,98s,AJo,KQo guess 55% actual 55%
     
**Supplemental Material:**
  - Online equity calculator from [openpokertools.com](https://openpokertools.com/range-equity.html)

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key points here in the template. List any analysis you did of the material and where the output is. Record any followup questions you asked and how you answered them if you did.

**Key Points**:
 - Equity is the percentage of the pot you are entitled to based upon your chance of winning the hand at showdown.
 - Hand vs Hand equity is calculated by averaging wins/tie/loss counts across all run outs
 - Hand vs Range and Range vs Range equities simply average across all hands in the range.
 - It's important to work with ranges that come up in real situations in equity calculators to develop intuition, so you can estimate equity in real situations.
**Analysis list**:
**Followup Questions**:
- Identify where the ranges used in the range vs range exercises might arise in actual play.

## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

  Find some preflop all-ins from your sessions and estimate your opponents range. Find the equity of your hand vs that range and then compare to the equity of hand vs hand based on what they actually had.

**Sessions:**

## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine  if you made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:**
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and do another session. Once you get an "A", review your conclusions about the techniques learned.
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Sessions and Grades:**
**Review and Conclusions:**

