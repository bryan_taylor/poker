# [Insert Title of Content]
## Study Template

This is a markdown template adapted from Sky Matsuhashi's [7-Step Process](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/) for getting the most from poker training content. Source code is [here](https://gitlab.com/bryan_taylor/poker).


## 1. Skim
  Skim the content, identify major concepts

  **Output:** Copy and customize this template, fill in book, chapter, concepts below:

**Book**:

**Chapter**:

**Concepts**:


## 2. Goals
  Set chapter goals.

  **Output**: Answer these 3 questions in this template

Questions:
 - What skills can I learn from this chapter?
 - Why are they important or relevant to my game?
 - How can I implement them in my game?

## 3. Read and Watch
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic from books, videos, blogs or other sources.

  **Output**: Fill in the date finished and list any learning outputs like chapter quizzes/exercises/activites

**Date finished:**
**Output list:**
**Supplemental Material:**

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key points here in the template. List any analysis you did of the material and where the output is. Record any followup questions you asked and how you answered them if you did.

**Key Points**:
**Analysis list**:
**Followup Questions**:


## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

**Sessions:**

## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine  if you made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:**
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and do another session. Once you get an "A", review your conclusions about the techniques learned.
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Sessions and Grades:**
**Review and Conclusions:**

