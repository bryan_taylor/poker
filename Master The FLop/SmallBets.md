# Template for How to Learn From Poker Strategy Books
https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/

## 1. Skim
  Skim the content, identify major concepts

  **Output:** Copy and customize this template, fill in book, chapter, concepts below:

**Video**: Master The Flop
**Chapter**: Small Bets
**Concepts**:
  - Small bet bluff to fold out high cards
  - Small bet for value wider
  - The "small bet, weak board == capped range" exploit

## 2. Goals
  Set chapter goals.

  **Output**: Answer these 3 questions in this template

Questions:
 - What skills can I learn from this chapter?
    - Do's and Don't of small cbets on the flop
 - Why are they important or relevant to my game?
    - Small bet bluffs fold out high card hands
    - Small bet value bluffs make money with weak top and strong 2nd pair
    - The "small bet, web board => capped range" exploit
 - How can I implement them in my game?
    - Bet small in the right circumstances for value and bluffs
    - Avoid it in the wrong circumstances
    - Know the play for the turn: small value, barrelling, checking for river

## 3. Read and Watch
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic from books, videos, blogs or other sources.

**Date finished:** 11/22/2021

**Output list:**
  Took the Upswing Bet Qizing [Quiz](https://upswingpoker.com/bet-sizing-quiz/) twice. Last result 9/10.

**Supplemental Material:**
  - Upswing Poker's [How to Choose Between Small vs Big Bet Sizes](https://upswingpoker.com/small-vs-big-bets/) blog
  - Upswing Poker's [https://upswingpoker.com/bet-size-strategy-tips-rules/](https://upswingpoker.com/bet-size-strategy-tips-rules/)

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key poits here in the template. Also list any analysis you did and where the output is

**Key Points**:
  - Use small cbets for:
    - cheaper draws and bluffs
    - easy to call value bets on dry boards "bleed them"
      - weak pairs and strong 2nd pairs can value bet small vs wider calling ranges
      - alternative to "pot control" check with strong pair, overpair
    - get called wider on dry boards so double barrel to fold A-high and gutshots
  - On wet flop, premiums should bet large to charge draws
    - If you cbet small, you split and cap your range
    - Exploit such cbets by playing better marginal value harder (promote them to premium)
    - small bet draws to "buy the river" vs opponents oblivious to this exploit
  - Ask how to fold out your opponent's overcard hands
    - Don't cbet bluff if this can't work
    - Consider how the flop hits his range based on his preflop action
    - Think about what your hand blocks. If you block overcards, cbet less often
  - Betting small in multiway pots works for any top pair, draw
    - Somebody probably hit, bluffs don't work
    - Raises, generally honest, so folding to better hands is easy
    - Check call marginal pairs with some equity
  - Use Flopzilla for small bet deliberate practice
    - Set up spots in flopzilla to see if you can fold his high cards
    - Find "worst case" spots where it still works

**Analysis list**:
**Followup Questions**:


**Followup Questions**:
 - **Q:** When is it good to have multiple actions with parts of our range vs one action for the whole range?
 - **Q:** How do small bets relate to "pot control"?
   **A:** A couple small bets are roughly equivalent to a checked back street in terms of the final pot size at showdown. Small bets encourage our opponent to play passively and wide, and require a bigger hand to wrest the initiative from us by raising us. So it can often be preferable to bet a lower strength premium hand small instead of checking a street for pot control.

## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

**Sessions:**
 - 10/22/2021 WPN "Broughton" 2NL - 232 hands
 - 10/22/2021 WPN "Heighworth" 2NL - 137 hands

## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:** "Broughton" TBD
   - Key Point:
     - Hand:
 - **Session:** "Heighworth" TBD
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and add another session
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Session Grade:**
**Review and Conclusions:**

