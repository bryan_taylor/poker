# Template for How to Learn From Poker Strategy Books
https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/

## 1. Skim
  Skim the content, identify major concepts

  **Output:** Copy and customize this template, fill in book, chapter, concepts below:

**Video Series**: 3 Betting Challenge
**Chapter**: Day 1: 3 Betting Concepts
**Concepts**:
  - Linear & Polarized Ranges
  - When you 3bet and get called
  - Facing a 3bet
  - Postflop when you call a 3bet


## 2. Goals
  Set chapter goals.

  **Output**: Answer these 3 questions in this template

Questions:
 - What skills can I learn from this chapter?
   - What hands/ranges/situations to 3bet with
   - How to respond when I am 3bet
   - How to play postflop in 3bet pots as 3better or caller
 - Why are they important or relevant to my game?
   - Lots of money can change hands in 3bet hands
 - How can I implement them in my game?
   - Improve my 3betting ranges
   - Improve my 3bet calling

## 3. Read
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic.

  **Output**: Fill in the date finished and list any learning outputs like chapter quizzes/exercises/activites

**Date finished:** 11/23/2021
**Output list:**
**Supplemental Material:**

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key poits here in the template. List any analysis you did of the material and where the output is. Record any followup questions you asked and how you answered them if you did.

**Key Points**:
  - Prefer a linear range when opponent call a lot
    - opponent starts with strong range
    - opponent tends to call to much (calling station / low fold-to-3bet)
    - opponent will have position
  - Prefer a polarlized range if opponent calls less (4bets or folds more)
    - shallow stacks (in tournaments or cash vs short stackers)
    - you have position
    - your opponent folds to 3bet often

**Analysis list**:
**Followup Questions**:


## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

**Sessions:**

## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:**
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and add another session
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Session Grade:**
**Review and Conclusions:**

