# Equity and EV
## Study Template

This is a markdown template adapted from Sky Matsuhashi's [7-Step Process](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/) for getting the most from poker training content. Source code is [here](https://gitlab.com/bryan_taylor/poker).


## 1. Skim
  Skim the content, identify major concepts

**Course**: From The Ground Up, by Peter Clarke, Run It Once
**Section**: Getting Started & Preflop
**Video**: Equity and EV (an introduction)
**Concepts**:
  - Equity  
  - Expected Value (EV)


## 2. Goals
  Set chapter goals.

**Questions:**
 - **What skills can I learn from this chapter?**
   - How to use equity as a numeric measure of how well my hand does if it gets to showdown.
   - How to use EV to predict how much money a given action will make, on average.
 - **Why are they important or relevant to my game?**
   - Equity measures my hand strength in the current situation under the assumption that no one folds and we reach showdown. When betting actions and sizes to come are uncertain, equity measures the fraction of the eventual pot that should come to us on average, at showdown.
   - Excepted Value is a prediction of the money won, on average, across all variables including board run outs, the opponents cards, and future actions and bet sizes, based on player strategies.
 - **How can I implement them in my game?**
   - Ultimately we want to select actions and bet sizes to maximize EV. 
   - Equity gives us a sense of where we stand at the moment.
   - Both are theoretical concepts we use during study to train our decision making.

## 3. Read and Watch
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic from books, videos, blogs or other sources.


**Date finished:** 4/5/2023
**Output list:** n/a
**Supplemental Material:**
 - [openpokertools.com Equity Calculator](https://openpokertools.com/range-equity.html) can evaluate hand and range equity
 - [GTOWizard](http://gtowizard.com) will show EV when all players play GTO (in lines where GTO is calculable)
 - [PokerSnowie](https://pokersnowie.com/) will show EV assuming all players play like a strong AI (in more lines than GTO can be calculated for)

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key points here in the template. List any analysis you did of the material and where the output is. Record any followup questions you asked and how you answered them if you did.

**Key Points**:
  - Equity: the percentage of the time our hand will win the pot at showdown if we are able to see all remaining cards (flop, turn, river)
  - EV: Expected Value is the average monetary outcome from taking a certain action.

**Analysis list**: n/a
**Followup Questions**:
  1. Why is it easier to calculate equity than EV in most situtations?
  2. Under what conditions can we calculate EV if we know equity?
  3. What are some reasons why EV of an action may represent more or less than the equity stake of the final pot?
  4. Many HUDs can show "All In EV" that modifies actual results. How is this calculated? How is it useful?

## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

Find hands in your session where you went all in. Calculate your equity and EV once you know your opponents hand.

**Sessions:** 
  - **TBD**


## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine if you made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:**
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and do another session. Once you get an "A", review your conclusions about the techniques learned.
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Sessions and Grades:**
**Review and Conclusions:**

