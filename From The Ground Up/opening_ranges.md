# Opening Ranges by Postion
## Study Template

This is a markdown template adapted from Sky Matsuhashi's [7-Step Process](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/) for getting the most from poker books. Source code is [here](https://gitlab.com/bryan_taylor/poker).


## 1. Skim
  Skim the content, identify major concepts


**Course**: From The Ground Up, by Peter Clarke, Run It Once

**Section**: Getting Started & Preflop

**Video**: Opening Ranges by Postion

**Concepts**:
 - Raise First In Ranges for HJ, LH, CO, BTN in 6max


## 2. Goals
  Set chapter goals.

Questions:
 - What skills can I learn from this chapter?
   - Default RFI opening ranges for HJ, LH, CO, BTN in 6max
   - How to think about exploitative adjustments to default ranges
 - Why are they important or relevant to my game?
   - We want a baseline default range, to compare other players to
   - We want a baseline default range to play ourselves and from which to modify for exploitative reasons
 - How can I implement them in my game?

## 3. Read and Watch
  Read and take notes. Do any chapter quizzes, excercises, or activites. Search up and list any supplemental material on the topic from books, videos, blogs or other sources.

**Date finished:** 4/6/2023
**Output list:**
**Supplemental Material:**
 - GTOWizard [Preflop Training Video](https://www.youtube.com/watch?v=MPae2gqkeRw)

## 4. Summarize and Analyze
  Summarize key points. If applicable, analyze using poker tools to verify what you learned.

  **Output**: Fill out a summary of key points here in the template. List any analysis you did of the material and where the output is. Record any followup questions you asked and how you answered them if you did.

**Key Points**:
  - Play all and only starting hands that are +EV after rake, varying by seat
  - We need to play ranges that are defensible against strong players who may 3bet us and use position to deny equity, especially in earlier positions
  - We want to able to profitably (+EV) continue with at least half our range when 3bet
  - EV of opening hands depends on opponent strategies. We dynamically adjust to the table.
  - When players that follow are aggresive 3bettors, we tighten
  - When players behind are tight or passive, we can steal more
**Analysis list**:
  - RFI ranges, plotted with [pokercoaching.com Range Analyzer](https://pokercoaching.com/range-analyzer/)
  ![ranges](img/Clarke_FTGU_RFI.png)
**Followup Questions**:


## 5. Take Action
  1-2 table focus sessions and work on adding one skill
  Output: list sessions, with results and saved hand histories

**Sessions:**

## 6. Assessment
  Do a hand history review of the new focus session. Use tools to determine  if you made good plays & decisions per learning key points
  Output: organized by key point from step 4, identify hands illustrate them and give each point/hand pair a score (1-100).

 - **Session:**
   - Key Point:
     - Hand:

## 7. Rince, Repeat, Review
  Based on latest focus sessions, give yourself a letter grade. If it's not an "A" or higher, go back to step 5 and do another session. Once you get an "A", review your conclusions about the techniques learned.
  **Output:** Give a letter grade for the most recent focus session, based on the score

**Sessions and Grades:**
**Review and Conclusions:**

