# Study Templates for Peter Clarke's From the Ground Up

This folder organizes study templates for Peter Clarke's [From the Ground Up](https://www.runitonce.com/courses/from-the-ground-up/) video course on the Run It Once training site. The course is $50.

Table of Contents

- Getting Started & Preflop
  - [Equity and EV (an introduction)](equity_EV.md)
  - [Opening Ranges by Position](opening_ranges.md)

