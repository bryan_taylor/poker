# Holdem Questions and Exercises

How would Socrates teach poker? Probably using the Socratic method, meaning he would ask questions only. This set of questions and exercises is designed to systematically break Texas Holdem down into a coherent body of knowledge. You have to provide the answers:

These are focused on 6max cash, typically 100BB deep.

## Preflop Raise First In strategy

1. When it is unopened to you, preflop, what factors influence if you will raise, call, or fold?
1. What are your opening ranges: Raise First In and limp ranges by seat:
    - UTG
    - HJ
    - CO
    - BTN
1. Exercise: Go to the [Range Analyzer](https://pokercoaching.com/range-analyzer/) and draw your UTG/HJ/CO/BTN ranges in red/blue/green/black. Score yourself 100 minus 1 for each hand/street combo you would play wrong and give yourself a letter grade.
1. What stats does a HUD gather that characterize a player's preflop play?
1. Exercise: user your HUD to find your values for these stats
1. What betsize do you use for your raises?
  - RFI UTG,HJ,CO,BTN
  - RFI SB
  - 3bet In position (IP)
  - 3bet Out of position (OOP)
7. Define your raise/call/fold strategy facing an RFI for each of the following situations:
  - Early/Middle:
    - HJ facing UTG RFI
    - CO facing UTG RFI
    - CO facing HJ RFI
 - Button:
    - BTN facing UTG RFI
    - BTN facing HJ RFI
    - BTN facing CO RFI
 - Small Blind:
    - SB facing UTG RFI
    - SB facing HJ RFI
    - SB facing CO RFI
    - SB facing BTN RFI
8. Exercise: Quiz yourself over early/middle, BTN and SB play facing an RFI with the Range Analyzer scoring as per #3
 1. If you are the SB and it's folded to you, how do you open? What are your RFI and limp ranges?
 1. How do you respond to limpers in front of you? As HJ, CO, BTN, SB?
 1. Define your Big Blind play call/raise ranges for:
   - BB facing UTG RFI
   - BB facing HJ RFI
   - BB facing CO RFI
   - BB facing BTN RFI
   - BB facing SB RFI
   - BB facing SB open limp
   - BB facing other, possibly multiple limps
12. Exercise: Quiz yourself over BB ranges with the Range Analyzer, scoring as per #3
1. Exercise: Using your poker tracking software identify all the hands from a session where:
  - You were RFI when you shouldn't have been
  - You folded or limped when you should've been RFI
  - You called an RFI when you should've folded
14. Define these preflop HUD statistics:
  - VPIP (voluntarily put in pot)
  - PFR (preflop raise)
  - RFI% (raise first in percentage)
  - Attempt to Steal %
  - Fold to Steal %
15. Exercise: For your HUD, find out how to see the above stats during play (or review), including by position.

## Concepts and Terminology

### Opponent Types
16. In terms of general behavior patterns how would you define these player labels?
  - nit
  - TAG (tight aggressive)
  - Tight Passive
  - GTO (game theory optimal)
  - LAG (loose aggressive)
  - Loose Passive
17. How would you apply preflop HUD stats to classify players preflop play with these labels?
1. Why do we usually use preflop (as opposed to postflop) attributes to assign these labels?
1. How do you define a "maniac"? Is this based on preflop or postflop behavior?
1. Are there other player types that are useful to name?
1. What HUD stats are relevant to classifying players preflop opponent type?
1. Create a color mapping to the opponent types for your Heads Up Disply (HUD's) allowed player colors.
1. Exercise: Review a session and colorize all the opponents
1. Exercise: Play an online session and colorize opponents as you play

### Ranges
25. What is a polarized range?
1. What is a linear range?
1. What is a capped range?
1. For positions where we only open raise and never call, is our range linear or polarized? Why?
1. For positions like the button and BB where call and raise are both options, which ranges (calling or raising) might be linear and which might be polarized?

### Equity and Hand Strength
30. Define the equity of one starting hand vs another.
1. Define the equity of one hand vs another with a given flop or turn.
1. When someone bets or raises, how do we define fold equity?
1. What does it mean for a hand to "realize" its equity?
1. How do you define these hand categories, generally:
  - premium hand
  - marginal hand
  - drawing hand
  - trash
35. Apply the hand categories preflop:
  - Which hand categories can open raise?
  - Which hand categories can call when facing a raise first in?
  - Which hand categories can raise when facing a raise first in?
36. Define the equity of one starting hand vs a range.
1. Define the equity of a starting hand range vs another.
1. Define the equity of one range of hands vs another given a flop or turn.
1. Define having a range advantage?
1. Define having a nut advantage?
1. Define the equity when called of a starting hand in a given position.
1. What's different about equity on the river?
  - What is the equity of one hand vs another on the river?
  - What is the equity of one hand vs a range on the river?
  - What is the equity of one range vs another on the river?
43. What are some tools that can be used to calculate equities for hands and ranges?

### Aggression

44. What are the valid reasons to bet or raise?
1. Which reason in #36 doesn't apply on the river?
1. What do these terms mean when betting:
  - value bet
  - thin value bet
  - bluff
  - semi-bluff
45. Which of the above bets aren't possible on all streets? Why?
1. On the river, with no draws, how do we modify the hand categories?
1. When we bet a linear range, which hand categories (from #32 or #46) might we bet? What factors influence the answer?
1. When we bet a polarized range, which categories are usually included?

### Responding to Aggression
51. When our opponent leads with a bet, which categories might have hands that:
  - raise
  - call
  - fold
52. When we check a hand because it isn't in our polarized betting range and an opponent bets, which categories might hands that take these betting lines come from:
  - check/raise
  - check/call
  - check/fold
53. When we check a hand because it isn't in our linear betting range and an opponent bets, which categories might hands that take these betting lines come from:
  - check/raise
  - check/call
  - check/fold

### Bluffing and Semi-Bluffing
54. What factors influence our decision to bluff or semi-bluff?
1. On the river, if the potsize is P, our betsize is S, we have V combos of value hands and B combos of bluffs, what relationship should hold between P, S, V, and B to maximize our profit?
1. What bluff-to-value ratio should we use on the flop and turn?
1. What factors influence which hands we should bluff with?
1. Which categories of hands do we look to first to find our bluffs?
1. If we can't find enough bluffs in the categories from #56 to make the ratios we want, what do we do?
1. When we check, if we consider hands we would check/fold to be bluffs, and the others value, what should our bluff-to-value ratio be?

## Preflop Responses

### Preflop Behavior by Opponent Types
61. What do you think the RFI ranges are for a typical nit?
  - UTG
  - HJ
  - CO
  - BTN
62. What do you think the RFI ranges are for a typical tag?
  - UTG
  - HJ
  - CO
  - BTN
63. What do you think the RFI ranges are for a typical lag?
  - UTG
  - HJ
  - CO
  - BTN
64. What do you think the RFI ranges are for a typical semi-loose passive?
  - UTG
  - HJ
  - CO
  - BTN
65. What do you think the RFI ranges are for a typical loose passive?
  - UTG
  - HJ
  - CO
  - BTN

### Preflop Adjustments Facing RFI
66. How do you adjust your calling and raising ranges facing an RFI from a nit?
1. How do you adjust your calling and raising ranges facing an RFI from a tag?
1. How do you adjust your calling and raising ranges facing an RFI from a tight pssive player?
1. How do you adjust your calling and raising ranges facing an RFI from a gto-like player?
1. How do you adjust your calling and raising ranges facing an RFI from a lag?
1. How do you adjust your calling and raising ranges facing an RFI from a semi-loose passive player?
1. How do you adjust your calling and raising ranges facing an RFI from a loose passive player?
1. How do you adjust your preflop play against a maniac?
1. How can you tell if a player is positionally aware?
1. If you conclude a player is not positionally aware how do you adjust your preflop play?


### 3bet, 4bet, 5bet
76. What factors influence if you will 3bet vs an RFI?
1. Is you standard 3bet range polarized or linear? Why?
1. What factors influence the decision to 3bet with a polarized or linear range?
1. What factors cause you to narrow or widen your 3betting range?
1. What factors affect your 3bet bet sizing? What's standard and when do you deviate from it?
1. If you are facing raise and one or more calls in front of you, how will will you modify your 3bet/squeeze range compared to how you'd 3bet the initial raiser?
1. What factors influence if you will 4bet vs someone who 3bets you?
1. Define a "cold 4bet".
1. How is your cold 4bet range different from that when the 3bet is against your raise?
1. What factors influence if you will 5bet when someone 4bets you?
1. What hands will you stack of with preflop against regs?
  - 100BB deep
  - deeper
  - shallower
87. If a player is open shoving every hand or almost every hand, how will you play against them?
1. If a player is open shoving once per orbit, how will you play against them?
1. If a player open shoves, having done this no or few times before, how will you play against them?

## Flop Play

### Flop Textures

90. How many different flops are there?
1. What factors describe flop texture?
1. Define monotone, twotone, and rainbow flops. How many of each are there?
1. How many boards are tripple rank, paired, unpaired?
1. What are the different ways that a player can flop a straight?
1. What are the different ways that a player can flop an open ended straight draw?
1. What are the different ways that a player can flop a gutshot straight draw?
1. How many flops allow straights, open ended straight draws, gutshots, or none of these.
1. Create a way to name, classify and simplify the card ranks on a flop.
1. With whatever you came up with in #58, how many of each type are there?
1. Come up with a way to name/notate and classify all flops by rank, connectedness, suitedness, and paired-ness.

### General Hand Assessment

101. What does it mean for a flop to connect with a range?
1. How do you generally use flop texture to assess range advantage?
1. How do you generally use flop texture to assess the nut advantage?
1. Given that human beings can't do range vs range equity calculations in their head, what should we do to develop a sense for range advantages on different flop textures?
1. Exercise: review the hands from your session in descending order of pot size and first guess their flop equity advantage, and then use an equity tool to calculate it. Track your average deviation from the correct value.

### Flop Actions

106. What is a continuation bet (cbet) and why is it a common betting pattern?
1. What is a donk bet and why is it not a common betting pattern?
1. How would you define a small, medium, large, pot, and overbet bet sizings?
1. Which of these bet sizings are the most common for a cbet?
1. What factors influence cbet sizing?


### Heads Up Single Raised Pot (SRP)

111. Enumerate the different position pairs scenarios for a heads up single raise pot.
1. For each HU SRP scenrio, who has the range advantage before seeing the flop?
1. What 
1. What percent of the time should you cbet in position in a SRP?
1. What percent of the time should you cbet out of position in a SRP?