# Poker Study Notes

I created this repo so I could keep and organize my work notes as I studied poker. I pick various poker improvement content from a variety of sources, and I apply a learning methodology to the content and that produces artifacts I store here.

## Learning Methodology

The approach here is an adaptation of Sky Matsuhashi's [7-Step Process for getting the most from poker books](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/). I really love the content Sky produces and this methodology in particular is fantastic.

### Implemenation

I follow Sky's 7-Step Process as described above, with some minor modifications described below. When I do this, I produce a page on each piece of content. The page is in markdown, which is a super easy to learn textual format that is really easy to publish with HTML style formatting, images, and links.

I chose markdown, because it's sort of a wiki format without the wiki, it's very easy, raw markdown is still easy to read, it has tons of free tools, and it allows the documentation content to be treated like source code. This means open source collaboration techniques can be used here. People can fork the project, modify things, add their own content, and maybe give some back to me. Both gitlab and github will automatically render markdown stored in repositories, so this is a powerful and free way to publish studies.

### How to Use This

To use this you will need some softare tools for at least markdown and git. See below. Then:
 - Find some poker content
 - Install git and clone the git repo to your local machine, eg:
   ```git clone git@gitlab.com:bryan_taylor/poker.git```
   or
   ```git clone https://gitlab.com/bryan_taylor/poker.git```
   The first command is faster, but requires a gitlab.com account and for you to generate and upload an ssh public key. The second uses HTTPS and is easier but slower.
 - Make a folder in the repo for yourself and a subfolder for each named poker content you study, eg for a book, video series, or blog series
 - Make sure you are familiar with Sky Matsuhashi's [7-Step Study Methodology](https://www.smartpokerstudy.com/how-to-learn-from-poker-strategy-books-podcast-21/)
 - Copy the master template "7-Step Template.md" into the subfolder
 - Rename it to something meaningful, eg for each chapter, blog, or video
 - Use your markdown editor to document your work as you go through the 7 steps

### Modifications

I modified the Sky's original approach in a few modest ways, so that:
 - it applies Sky's 7 Step Process to a mix of written or video content
 - you are encouraged to index and use supplemental material in step 3
 - you capture followup questions and hopefully their answers during analysis in step 4
 - you give yourself a numeric and letter grade during focus sessions in steps 6 and 7
 - you do focus session until you get an A
 - you capture your conclusions about the tenique you learned (after you get an A)

## Tools

Here's what you'll need to do things this way:
 - A Markdown Editor
 - An account on a source code site like gitlab.com or github.com
 - A git client
 - Poker tools
 - Image capture and editing tools

There are many ways to edit markdown. I use visual studio code, because it's free, cross platform (mac, windows, linux), has free markedown extensions (I use Markdown Preview Enhanced) and I use it for software development. This might be overkill. There a ton of easy light weight mardown tools out there. Just google it. MacDown, Typora, ghostwriter are all popular. Online tools like [hackmd](https://hackmd.io/) are interesting.

 Markdown is the simplest form of "source code" imaginable, so we use source code management software for it, specifically git. Git has an enourmous feature set for software developers, but we won't need much of it. With git, code is in repositories. These can be cloned to pull them from a server to you. They can be modified locally via a "commit" and commits can be pushed or pulled to the server, perhaps in branches for isolation of changes until they are merged. Using git for this is easy.

We dont want to run our own server for git and we don't have to, because there are sites that do it for you. I use gitlab.com . It's free for open source content, which this is. Github.com is also very popular. Both render any markdown file to html automatically. This last point is key. We just upload our source code with git by pushing it, and these sites render it for display automatically.

I use git and the command line and a tool called Fork for my checkins. There's a bazillion resources and tools on how to use git for collaborading on code. Google it.

I capture images using Mac built in image capture. Hit ⌘-SHIFT-4 and draw a rectanlge and the enclosed area with be turned into a png on your desktop.

### Poker Tools

There are various tools to make it easy to study poker. Here's some I know of and like:
- Hand & Range Equity Calculators
  - Flopzilla
  - Equilab
- Hand Range Visualization (free)
  - pokercoaching.com's [Range Analyzer](https://pokercoaching.com/range-analyzer/)
  - [Open Poker Tools](https://openpokertools.com/)
- GTO Solvers
  - GTO+ (works nicely with Flopzilla)
  - PioSolver
  - MonkerSolver
- Poker Databases and Heads Up Displays (HUD)
  - PokerTracker4
  - HoldemManager
  - DriveHUD

